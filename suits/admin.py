from django.contrib import admin
from django.http import HttpResponse
from django.contrib.admin import ModelAdmin
from django.forms import ModelForm, CharField

from import_export import resources
from import_export.forms import ExportForm
from import_export.admin import ExportMixin
from suit.admin import SortableTabularInline
from suit.widgets import AutosizedTextarea, TextInput
from django.template.response import TemplateResponse

from .models import Step, StepsBundle, Case, Suit, Runner, Project


class MyExportMixin(ExportMixin):

    def export_action(self, request, *args, **kwargs):
        formats = self.get_export_formats()
        form = ExportForm(formats, request.POST or None)
        if form.is_valid():
            file_format = formats[
                int(form.cleaned_data['file_format'])
            ]()

            resource_class = self.get_export_resource_class()
            queryset = self.get_export_queryset(request)
            data = resource_class().export(queryset)
            response = HttpResponse(
                file_format.export_data(data),
                content_type='application/octet-stream',
            )
            response['Content-Disposition'] = 'attachment; filename=%s' % (
                self.get_export_filename(file_format),
            )
            return response

        context = {}
        context['form'] = form
        context['opts'] = self.model._meta
        return TemplateResponse(request, [self.export_template_name],
                                context, current_app=self.admin_site.name)


#actions
def make_successed(modeladmin, request, queryset):
    queryset.update(is_success=True)
make_successed.short_description = "Mark selected cases as successful"


def make_unsuccessed(modeladmin, request, queryset):
    queryset.update(is_success=False)
make_unsuccessed.short_description = "Mark selected cases as unsuccessful"


class StepResource(resources.ModelResource):

    class Meta:
        model = Step


#models
# class StepAdmin(ModelAdmin):
class StepAdmin(MyExportMixin, ModelAdmin):
    resource_class = StepResource
    search_fields = ('name', )


class StepBundleResource(resources.ModelResource):

    class Meta:
        model = StepsBundle


class StepsBundleAdmin(MyExportMixin, ModelAdmin):
    resource_class = StepBundleResource
    search_fields = ('description', )
    list_display = ('description', )


class StepsBundleInline(SortableTabularInline):
    model = StepsBundle
    sortable = 'order'
    extra = 1


class CaseResource(resources.ModelResource):

    class Meta:
        model = Case


class CaseAdmin(MyExportMixin, ModelAdmin):
    inlines = (StepsBundleInline, )
    search_fields = ('name', )
    list_display = ('name', 'is_success')
    actions = [make_successed, make_unsuccessed]


class CaseInlineForm(ModelForm):
    scenario = CharField(label='Scenario', widget=AutosizedTextarea(attrs={'readonly': 'readonly'}), required=False)

    def __init__(self, *args, **kwargs):
        super(CaseInlineForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            desc_list = list('{0}. {1}\n -- {2}'.format(i, v['step__description'], v['description'])
                             for i, v in enumerate(self.instance.steps.values('step__description', 'description')))
            self.initial['scenario'] = '\n'.join(desc_list)

    class Meta:
        model = Case
        fields = [
            'name',
            'description',
            'precondition',
            'scenario',
            'expected_results',
            'results',
            'is_success',
            'order'
        ]
        widgets = {
            'name': TextInput(attrs={'style': 'width: 150px'}),
            'description': AutosizedTextarea(attrs={'style': 'width: 150px'}),
            'precondition': AutosizedTextarea(attrs={'style': 'width: 150px'}),
            'expected_results': AutosizedTextarea(attrs={'style': 'width: 150px'}),
            'results': TextInput(attrs={'style': 'width: 150px'}),
        }


class CaseInline(SortableTabularInline):
    model = Case
    form = CaseInlineForm
    sortable = 'order'
    extra = 1
    
    def __init__(self, *args, **kwargs):
        super(CaseInline, self).__init__(*args, **kwargs)
        i = 0


class SuitResource(resources.ModelResource):

    class Meta:
        model = Suit


class SuitAdmin(MyExportMixin, ModelAdmin):
    inlines = (CaseInline, )
    search_fields = ('name', )


class SuitInline(SortableTabularInline):
    model = Suit
    sortable = 'order'
    extra = 1


class RunnerResource(resources.ModelResource):

    class Meta:
        model = Runner


class RunnerAdmin(MyExportMixin, ModelAdmin):
    resource_class = RunnerResource
    inlines = (SuitInline, )
    search_fields = ('name', )


class RunnerInline(SortableTabularInline):
    model = Runner
    sortable = 'order'
    extra = 1


class ProjectResource(resources.ModelResource):

    class Meta:
        model = Project


class ProjectAdmin(MyExportMixin, ModelAdmin):
    resource_class = ProjectResource
    inlines = (RunnerInline, )
    search_fields = ('name', )

#register
admin.site.register(StepsBundle, StepsBundleAdmin)
admin.site.register(Step, StepAdmin)
admin.site.register(Case, CaseAdmin)
admin.site.register(Suit, SuitAdmin)
admin.site.register(Runner, RunnerAdmin)
admin.site.register(Project, ProjectAdmin)
