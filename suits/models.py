from django.db import models
from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

DEFAULT_USER_ID = 1


class Project(TimeStampedModel):
    name = models.CharField(default='', max_length=255)
    description = models.CharField(default='', max_length=255, blank=True)

    def __unicode__(self):
        return 'Name: {0}'.format(self.name)

    class Meta:
        ordering = ['name']


class Runner(TimeStampedModel):
    project = models.ForeignKey(Project, related_name='runners')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, default=DEFAULT_USER_ID)
    name = models.CharField(default='', max_length=255)
    description = models.CharField(default='', max_length=255, blank=True)
    order = models.PositiveIntegerField(default=0)

    def display_scenario(self):
        return

    def __unicode__(self):
        return 'Name: {0} Project: {1}'.format(self.name, self.project.name)

    class Meta:
        ordering = ['name']
        verbose_name = _('Run')
        verbose_name_plural = _('Runs')


class Suit(TimeStampedModel):
    runner = models.ForeignKey(Runner, related_name='suits', verbose_name=_('run'))
    name = models.CharField(default='', max_length=255)
    description = models.CharField(default='', max_length=255, blank=True)
    order = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return 'Name: {0} Run: {1}'.format(self.name, self.runner.name)

    class Meta:
        ordering = ['name']


class Case(TimeStampedModel):
    suit = models.ForeignKey(Suit, related_name='cases')
    name = models.CharField(default='', max_length=255)
    description = models.CharField(default='', max_length=255, blank=True)
    precondition = models.CharField(default='', max_length=255, blank=True)
    expected_results = models.CharField(default='', max_length=255, blank=True)
    results = models.CharField(default='', max_length=255, blank=True)
    is_success = models.BooleanField(default=False)
    order = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return 'Name: {0}'.format(self.name)

    class Meta:
        ordering = ['name']


class Step(TimeStampedModel):
    name = models.CharField(default='', max_length=255)
    description = models.CharField(default='', max_length=255, blank=True)

    def __unicode__(self):
        return 'Name: {0}'.format(self.name)

    class Meta:
        ordering = ['name']


class StepsBundle(TimeStampedModel):
    case = models.ForeignKey(Case, related_name='steps')
    step = models.ForeignKey(Step, related_name='bundles')
    description = models.CharField(default='', max_length=255, blank=True)
    order = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return 'Suit: {0}'.format(self.case.name)

    class Meta:
        ordering = ['order']