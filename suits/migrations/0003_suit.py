# encoding: utf8
from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('suits', '0002_project_runner'),
    ]

    operations = [
        migrations.CreateModel(
            name='Suit',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name=u'created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name=u'modified', editable=False, blank=True)),
                ('runner', models.ForeignKey(to='suits.Runner', to_field=u'id')),
                ('name', models.CharField(default='', max_length=255)),
                ('description', models.CharField(default='', max_length=255, blank=True)),
                ('order', models.PositiveIntegerField(default=0)),
            ],
            options={
                u'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
    ]
