# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('suits', '0005_auto_20140415_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='runner',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1, to_field=u'id'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='suit',
            name='runner',
            field=models.ForeignKey(to='suits.Runner', to_field=u'id', verbose_name=u'run'),
        ),
    ]
