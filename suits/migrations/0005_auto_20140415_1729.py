# encoding: utf8
from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('suits', '0004_case'),
    ]

    operations = [
        migrations.CreateModel(
            name='StepsBundle',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name=u'created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name=u'modified', editable=False, blank=True)),
                ('case', models.ForeignKey(to='suits.Case', to_field=u'id')),
                ('step', models.ForeignKey(to='suits.Step', to_field=u'id')),
                ('description', models.CharField(default='', max_length=255, blank=True)),
                ('order', models.PositiveIntegerField(default=0)),
            ],
            options={
                u'ordering': ['order'],
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='RunnersBundle',
        ),
        migrations.DeleteModel(
            name='Projects',
        ),
        migrations.AlterField(
            model_name='step',
            name='description',
            field=models.CharField(default='', max_length=255, blank=True),
        ),
    ]
