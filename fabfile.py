__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

import os
import re

from fabtools import require
from fabric.contrib import files
from fabric.api import env, run, cd, prefix, local, settings, put, sudo, shell_env

DEFAULT_SYSTEM_USER = 'root'
DEFAULT_APP_USER = 'scenario'
ROOT_DIRECTORY = '/srv/{0}/'.format(DEFAULT_APP_USER)
DJANGO_SETTINGS_MODULE = os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{0}.settings".format(DEFAULT_APP_USER))

LOCAL_PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__)))

REQUIRE_PACKAGES = [
    'git-core',
    'python-dev',
    'build-essential',
    'python-pip',
    'mysql-client',
    'libmysqlclient-dev',
    'libjpeg-dev',
    'libfreetype6-dev',
    'libfreetype6',
    'nginx',
    'supervisor',
]

# environment settings
env.project_root = ROOT_DIRECTORY
env.ve = os.path.join(env.project_root, '.ve/bin/activate')
env.key_filename = []

key_path = os.path.join(os.environ['HOME'], '.ssh', 'id_rsa.pub')
if os.path.exists(key_path):
    env.key_filename.append(key_path)

if not env.hosts:
    env.hosts = ['127.0.0.1']


class forward_agent:
    def __enter__(self):
        env.forward_agent = True
        output = local('ssh-agent', capture=True)
        for sh_line in output.splitlines():
            matches = re.search("(\S+)=(\S+);", sh_line)
            if matches:
                os.environ[matches.group(1)] = matches.group(2)

        key_path = os.path.join(os.environ['HOME'], '.ssh', 'id_rsa')
        local('ssh-add "{0}"'.format(key_path))

    def __exit__(self, *args, **kwargs):
        env.forward_agent = False
        local('kill {0}'.format(os.environ['SSH_AGENT_PID']))


def enable_forwarding(function=None):

    def decorator(view_func):
        def _wrapper(*args, **kwargs):
            with forward_agent():
                return view_func(*args, **kwargs)
        return _wrapper

    return decorator(function) if function else decorator


def _setup_libs(system_user):
    env.user = system_user
    sudo('apt-get update >> /dev/null')
    require.deb.packages(REQUIRE_PACKAGES)
    sudo('pip install --upgrade pip')
    sudo('pip install --upgrade virtualenv')
    print 'All required libraries are installed.'


def _setup_user(new_user, system_user):
    env.user = system_user
    if run('id {0}'.format(new_user), quiet=True).return_code:
        path = '/home/{0}'.format(new_user)
        ssh_path = '{0}/.ssh'.format(path)

        sudo('useradd {0} -m -s /bin/bash'.format(new_user))
        sudo('[ -d {0} ] || mkdir {0}'.format(ssh_path))
        with cd(ssh_path):
            sudo('echo -e "Host github.com\n\tStrictHostKeyChecking no\n" >> config')
            put(env.key_filename[0], 'authorized_keys', use_sudo=True)
        sudo('chmod -R 700 {0}'.format(ssh_path))
        sudo('chown -R {0}:{0} {1}'.format(new_user, path))
    else:
        print 'User {0} already exists!'.format(new_user)


def _setup_db(new_user, system_user):
    env.user = system_user
    require.mysql.server(password=os.environ.get('DB_PASSWORD', ''))
    with settings(mysql_user='root', mysql_password=os.environ.get('DB_PASSWORD', '')):
        require.mysql.user(os.environ.get('DB_USER', ''), password=os.environ.get('DB_PASSWORD', ''))
        require.mysql.database(os.environ.get('DB_NAME', ''), owner=os.environ.get('DB_USER', ''))


def _setup_project_dir_structure(user, system_user):
    env.user = system_user
    sudo('[ -d {0} ] || mkdir {0}'.format(ROOT_DIRECTORY))
    sudo('chgrp {0} {1}'.format(user, ROOT_DIRECTORY))
    sudo('chown {0} {1}'.format(user, ROOT_DIRECTORY))
    sudo('mkdir -p /var/log/{0}/supervisor'.format(DEFAULT_APP_USER))
    sudo('mkdir -p /var/log/{0}/nginx'.format(DEFAULT_APP_USER))
    sudo('chmod -R 777 /var/log/{0}'.format(DEFAULT_APP_USER))


def _setup_nginx(project_name, system_user):
    env.user = system_user
    config_path = os.path.join(env.project_root, 'deploy')
    nginx_conf_path = os.path.join(config_path, 'nginx-{0}.conf'.format(DEFAULT_APP_USER))
    sudo('cp -f {0} /etc/nginx/sites-available/{1}.conf'.format(nginx_conf_path, project_name))
    sudo('ln -fs /etc/nginx/sites-available/{0}.conf /etc/nginx/sites-enabled/{0}.conf'.format(project_name))
    sudo('service nginx restart')


def _setup_supervisor(project_name, system_user):
    env.user = system_user
    with settings(warn_only=True):
        current_path = os.getcwd()
        files.upload_template(
            filename=os.path.join(current_path, 'deploy/supervisor-{0}.conf'.format(project_name)),
            destination='/etc/supervisor/conf.d/{0}.conf'.format(project_name),
            context={'settings': DJANGO_SETTINGS_MODULE}, backup=False, mode=0644, use_sudo=True
        )
        sudo('supervisorctl reread')
        sudo('supervisorctl update')


def _update_repository(requirements='production', default_user=DEFAULT_APP_USER):
    env.user = default_user
    import git
    repo = git.Repo()
    if run('[ -d "{0}" ]'.format(os.path.join(env.project_root, '.git')), quiet=True).return_code:
        run('git clone -b {0} {1} {2}'.format(repo.active_branch, repo.remotes.origin.url, env.project_root))
    with cd(env.project_root):
        run('git pull origin {0}'.format(repo.active_branch))
        context = {
            'debug': os.environ.get('DEBUG', True),
            'allowed_hosts': os.environ.get('ALLOWED_HOSTS', env.hosts),
            'secret_key': os.environ.get('SECRET_KEY', ''),
            'db_engine': os.environ.get('DB_ENGINE', 'django.db.backends.sqlite3'),
            'db_name': os.environ.get('DB_NAME', os.path.join(env.project_root, 'dev.db')),
            'db_user': os.environ.get('DB_USER', ''),
            'db_password': os.environ.get('DB_PASSWORD', ''),
            'db_host': os.environ.get('DB_HOST', ''),
            'db_port': os.environ.get('DB_PORT', ''),
            'db_options': os.environ.get('DB_OPTIONS', None),
        }
        files.upload_template(
            filename=os.path.join(os.getcwd(), 'deploy', 'local_settings'),
            destination=os.path.join(env.project_root, DEFAULT_APP_USER, 'local_settings.py'),
            context=context, backup=False, mode=0600
        )
        # remove pyc files
        run("find ./ -name *.pyc -exec rm {} \;")

        if run('[ -d "{0}" ]'.format(os.path.join(env.project_root, '.ve')), quiet=True).return_code:
            run('virtualenv --no-site-packages .ve')
        with prefix('source {0}'.format(env.ve)):
            run('pip install -Ur requirements/{0}.pip'.format(requirements))
            with shell_env(DJANGO_SETTINGS_MODULE=DJANGO_SETTINGS_MODULE):
                run('python manage.py collectstatic --noinput')
                run('python manage.py syncdb --noinput')
                run('python manage.py migrate')
                # run('python manage.py loaddata scenario/fixtures/sites.json')


def restart_application(app='all', system_user=DEFAULT_SYSTEM_USER):
    env.user = system_user
    sudo('supervisorctl restart {0}'.format(app))


def stop_application(app='all', system_user=DEFAULT_SYSTEM_USER):
    env.user = system_user
    sudo('supervisorctl stop {0}'.format(app))


def start_application(app='all', system_user=DEFAULT_SYSTEM_USER):
    env.user = system_user
    sudo('supervisorctl start {0}'.format(app))


# def run_local_tests():
#     local('[ ! -d reports ] || rm -R reports')
#     local('mkdir reports')
#     local('.ve/bin/pip install -Ur requirements/tests.pip')
#     local('.ve/bin/python manage.py test --settings=scenario.settings.tests')


def setup_environment(default_user=DEFAULT_APP_USER, system_user=DEFAULT_SYSTEM_USER):
    _setup_libs(system_user)
    _setup_db(default_user, system_user)
    _setup_user(default_user, system_user)
    _setup_project_dir_structure(default_user, system_user)


def upload_configs(project_name, system_user=DEFAULT_SYSTEM_USER):
    _setup_nginx(project_name, system_user)
    _setup_supervisor(project_name, system_user)


@enable_forwarding
def deploy(default_user=DEFAULT_APP_USER, system_user=DEFAULT_SYSTEM_USER):
    setup_environment(default_user, system_user)
    _update_repository('production', default_user)
    upload_configs(DEFAULT_APP_USER)
    restart_application()


@enable_forwarding
def total_repository_update(requirements='production', default_user=DEFAULT_APP_USER):
    stop_application()
    env.user = default_user
    run('find {0} -maxdepth 1 ! -path {0} -exec rm -Rf {{}} \;'.format(env.project_root))
    _update_repository(requirements, default_user)


@enable_forwarding
def update(requirements='production', default_user=DEFAULT_APP_USER):
    _update_repository(requirements, default_user)
    restart_application()
